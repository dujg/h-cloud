#!/bin/bash
#应用信息
appDir=hcloud/audit
appName=hcloud-audit-web-1.0
xmx=512m
appProfile=prod
logDate=`date +%Y%m%d`

#杀掉旧进程
PID=$(ps -ef | grep java | grep $appName | grep -v grep | awk '{print $2}')
if [ -z "$PID" ]
then
    echo Application is already stopped
else
	echo kill $PID
	kill -9 $PID
fi

#起服务
nohup java -Xmx$xmx -jar $appDir/$appName.jar --spring.cloud.config.profile=$appProfile > logs/$appName.log 2>&1 &
#滚动启动日志
tail -f logs/$appName.log
