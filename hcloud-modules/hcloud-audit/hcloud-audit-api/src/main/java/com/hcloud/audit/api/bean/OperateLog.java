package com.hcloud.audit.api.bean;

import com.hcloud.common.core.base.BaseBean;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther hepangui
 * @Date 2018/11/20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class OperateLog extends BaseBean {
    private String username;
    private String operate;
    private Long time;
    private String ip;
    private String params;
    private String uri;
    private String type;
}
