package com.hcloud.auth.third;

import com.hcloud.auth.api.config.LoginType;
import com.hcloud.auth.config.SecurityConstants;

import javax.servlet.http.HttpServletRequest;

/**
 * @Auther hepangui
 * @Date 2018/12/13
 */
public class ThirdLoginUtil {
    public static LoginType getLoginType(HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        if (requestURI.indexOf(SecurityConstants.LOGIN_MOBILE) > -1) {
            return LoginType.mobile;
        } else if (requestURI.indexOf(SecurityConstants.LOGIN_QQ) > -1) {
            return LoginType.qq;
        } else if (requestURI.indexOf(SecurityConstants.LOGIN_WEIXIN) > -1) {
            return LoginType.weixin;
        }else if (requestURI.indexOf(SecurityConstants.LOGIN_GITEE) > -1) {
            return LoginType.gitee;
        }
        return LoginType.pass;
    }

    public static String getParam(HttpServletRequest request, LoginType loginType) {
        String principal = null;
        switch (loginType) {
            case mobile:
                principal = request.getParameter(SecurityConstants.PARAM_MOBILE);
                break;
            case qq:
            case gitee:
            case weixin:
                principal = request.getParameter(SecurityConstants.PARAM_CODE);
                break;
        }
        if (principal == null) {
            principal = "";
        }
        return principal.trim();
    }
}
