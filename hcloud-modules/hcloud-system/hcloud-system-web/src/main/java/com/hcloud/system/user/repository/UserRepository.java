package com.hcloud.system.user.repository;

import com.hcloud.common.crud.repository.BaseRepository;
import com.hcloud.system.user.entity.UserEntity;
import org.springframework.stereotype.Repository;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Repository
public interface UserRepository extends BaseRepository<UserEntity> {
    UserEntity findByAccount(String account);

    UserEntity findByMobile(String mobile);

    UserEntity findByWechat(String wechat);

    UserEntity findByQq(String qq);
}
