package com.hcloud.system.authority.repository;

import com.hcloud.system.authority.entity.AuthorityEntity;
import com.hcloud.common.crud.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Repository
public interface AuthorityRepository extends BaseRepository<AuthorityEntity> {
    List<AuthorityEntity> findByIdIn(List<String> ids);
}
